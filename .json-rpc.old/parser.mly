%{
    open Ast
%}

%token <int> INT
%token <float> FLOAT
%token <string> STRING
%token <bool> BOOL
%token LSQUARE RSQUARE
%token LBRACKET RBRACKET
%token LPAREN RPAREN
%token COLON COMMA
%token EOF

%start <Ast.t list> main

%%

main:
| lst = expr+ EOF
    {
      lst
    }

message:
  | CONTENT_LENGTH COLON INT j = json
    {
      j
    }

json:
  | ass = assoc_array
    {
      ass
    }

value:
  | i = INT
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc @@  Int i
    }
  | f = FLOAT
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc @@ Float f
    }
  | b = BOOL
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc @@ Bool b
    }
  | s = STRING
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc @@ String s
    }
  | NULL
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc NULL
    }
  | ass = assoc_list
    {
      ass
    }
  | LSQUARE vl = list_fields RSQUARE
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc @@ List vl
    }

assoc_list:
  | LBRACKET obj = obj_fields RBRACKET
    {
      let loc = Location.make $startpos $endpos in
      Json.make ~loc @@ Assoc obj
    }

obj_fields:
    obj = separated_list(COMMA, obj_field)    { obj } ;

obj_field:
    k = STRING; COLON; v = value              { (k, v) } ;

list_fields:
    vl = separated_list(COMMA, value)         { vl } ;
