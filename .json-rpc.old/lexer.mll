{
  open Parser

  exception Error of string

}

(* This rule analyzes a single line and turns it into a stream of
   tokens. *)

rule token = parse
| '\n'
  { Lexing.new_line lexbuf; token lexbuf }
| [' ' '\t' '\r']
    { token lexbuf }
| '"' ( [^ '"' '\\'] | ( '\\' [^ '"'] ) ) '"'
    { STRING s  }
| ['0'-'9']+'.'['0'-'9']* as i
    { FLOAT (float_of_string i) }
| ['0'-'9']+ as i
    { INT (int_of_string i) }
| "true" { BOOL true } | "false" { BOOL false }
| "null" { NULL }
| '{' { LBRACKET } | '}' { RBRACKET }
| '[' { LSQUARE }  | ']' { RSQUARE }
| '(' { LPAREN }   | ')' { RPAREN }
| ':' { COLON } | ',' { COMMA }
| 'Content-Length' { CONTENT_LENGTH }
| _
    { raise (Error (Printf.sprintf "At offset %d: unexpected character.\n" (Lexing.lexeme_start lexbuf))) }
