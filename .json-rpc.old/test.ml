open Ligo_helpers.Trace

let test name f =
  Alcotest.test_case name `Quick @@ fun _sw ->
  match f () with
  | Ok () -> ()
  | Errors errs ->
    Format.printf "Errors :\n%!" ;
    List.iter (fun e -> Format.printf "%s\n" e.title) errs ;
    raise Alcotest.Test_error

let assert_equal a b =
  if a <> b
  then simple_fail "Not Equals"
  else ok ()

let parse lex_buf =
  try
    ok @@ Parser.main Lexer.token lex_buf
  with
  | Lexer.Error msg -> simple_fail msg
  | Parser.Error -> simple_fail @@ Format.sprintf "At offset %d: parse error.\n%!" (Lexing.lexeme_start lex_buf)

open Ast

let assert_equal_type ((a, b) : Typed.t * Typed.t) : Typed.ty result =
  if a.meta.ty = b.meta.ty
  then ok a.meta.ty
  else simple_fail "types not equal"

let pair_map f (x, y) = (f x, f y)

let rec type_check (unt:Untyped.t) : Typed.t result =
  let open Untyped in
  let open Typed in
  let loc = unt.meta in
  match unt.ast with
  | Literal (Skeleton.Int _) as ast -> ok @@ make ast loc Typed.Int
  | Literal (Skeleton.Float _) as ast -> ok @@ make ast loc Typed.Float
  | Literal (Skeleton.Bool _) as _ast -> simple_fail "not implemented yet"
  | Primitive (prim, lst) -> (
      let arity = arity prim in
      trace (simple_error "bad arity") @@ assert_equal arity (List.length lst) >>? fun () ->
      match prim with
      | Plus | Minus | Times | Div -> (
          trace (simple_error "first member can't be typed") @@ type_check @@ List.nth lst 0  >>? fun a ->
          trace (simple_error "second member can't be typed") @@ type_check @@ List.nth lst 1  >>? fun b ->
          trace (simple_error "both members don't have same type") @@ assert_equal_type (a, b) >>? fun ty ->
          ok @@ Typed.make (Skeleton.primitive prim [a;b]) loc ty
        )
      | Opposite -> (
          trace (simple_error "member can't be typed") @@ type_check @@ List.nth lst 0 >>? fun a ->
          ok @@ Typed.make (Skeleton.primitive prim [a]) loc a.meta.ty
        )
    )


module Value = struct
  type t =
    | Int of int
    | Float of float

  let apply f g = function
    | Int i -> Int (f i)
    | Float j -> Float (g j)

  let apply2 f g = function
    | Int i, Int j -> Int (f i j)
    | Float i, Float j -> Float (g i j)
    | _ -> failwith "impossible"
end

let rec eval (t:Typed.t) : Value.t result =
  let open Value in
  match t.ast with
  | Literal (Skeleton.Int i) -> ok @@ Int i
  | Literal (Skeleton.Float i) -> ok @@ Float i
  | Literal (Skeleton.Bool _) -> simple_fail "not implemented yet"
  | Primitive (prim, lst) -> (
      trace (simple_error "error in child eval") @@ sequence eval lst >>? fun lst ->
      match prim with
      | Plus ->
        let ab = (List.nth lst 0, List.nth lst 1) in
        ok @@ apply2 (+) (+.) ab
      | Minus ->
        let ab = (List.nth lst 0, List.nth lst 1) in
        ok @@ apply2 (-) (-.) ab
      | Times ->
        let ab = (List.nth lst 0, List.nth lst 1) in
        ok @@ apply2 ( * ) ( *.) ab
      | Div ->
        let ab = (List.nth lst 0, List.nth lst 1) in
        ok @@ apply2 (/) (/.) ab
      | Opposite ->
        let a = List.nth lst 0 in
        ok @@ apply (fun x -> 0 - x) (fun x -> 0. -. x) a
    )

let assert_eval str expected_result : unit result =
  let lex_buf = Lexing.from_string str in
  parse lex_buf >>? fun parsed ->
  type_check  parsed >>? fun typed ->
  eval typed >>? fun result ->
  assert_equal result expected_result

let operation () =
  assert_eval "(1 + 2 * 10) * 2\n" @@ Value.Int 42

let float () =
  assert_eval "42.\n" @@ Value.Float 42.

let main = "test.ml", [
    test "operation" operation ;
    test "float" float ;
  ]

let () =
  (* Printexc.record_backtrace true ; *)
  Alcotest.run "Parser" [
    main
  ] ;
  ()
