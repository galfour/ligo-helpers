type 'm ast =
  | Int of int
  | Float of float
  | Bool of bool
  | NULL
  | List of 'm t list
  | Assoc of (string * 'm t) list

and 'm t = {
  ast : 'm ast ;
  meta : 'm ;
}

let make ast meta = { ast ; meta }

module Location = struct
  type t = {
    filename : string ;
    start_line : int ;
    start_column : int ;
    end_line : int ;
    end_column : int ;
  }

  let make (start_pos:Lexing.position) (end_pos:Lexing.position) =
    let filename = start_pos.pos_fname in
    let start_line = start_pos.pos_lnum in
    let end_line = end_pos.pos_lnum in
    let start_column = start_pos.pos_cnum - start_pos.pos_bol in
    let end_column = end_pos.pos_cnum - end_pos.pos_bol in
    { filename ; start_line ; start_column ; end_line ; end_column }
end

type nonrec t = Location.t t
