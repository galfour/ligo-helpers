type error = {
  message : string ;
  title : string ;
}

type 'a result =
    Ok of 'a
  | Errors of error list

let ok x = Ok x
let fail err = Errors [err]

let simple_error str = {
  message = str ;
  title = str ;
}

let error title message = { title ; message }

let simple_fail str = fail @@ simple_error str

let map f = function
  | Ok x -> f x
  | Errors _ as e -> e

let apply f = function
  | Ok x -> Ok (f x)
  | Errors _ as e -> e

let (>>?) x f = map f x
let (>>|?) = apply

module Let_syntax = struct
  let bind m ~f = m >>? f
end

let trace err = function
  | Ok _ as o -> o
  | Errors errs -> Errors (err :: errs)

let trace_option error = function
  | None -> fail error
  | Some s -> ok s

let rec bind_list = function
  | [] -> ok []
  | hd :: tl -> (
      hd >>? fun hd ->
      bind_list tl >>? fun tl ->
      ok @@ hd :: tl
    )

module AE = Tezos_utils.Memory_proto_alpha.Alpha_environment
module TP = Tezos_base__TzPervasives

let trace_alpha_tzresult error =
  function
  | Result.Ok x -> ok x
  | Error _ -> fail error

let trace_alpha_tzresult_lwt error (x:_ AE.Error_monad.tzresult Lwt.t) : _ result =
  match Lwt_main.run x with
  | Result.Ok x -> ok x
  | Error _ -> fail error

let trace_tzresult error =
  function
  | Result.Ok x -> ok x
  | Error _ -> fail error

let trace_tzresult_lwt error (x:_ TP.Error_monad.tzresult Lwt.t) : _ result =
  match Lwt_main.run x with
  | Result.Ok x -> ok x
  | Error _ -> fail error

let generic_try err f =
  try (
    ok @@ f ()
  ) with _ -> fail err

let specific_try handler f =
  try (
    ok @@ f ()
  ) with exn -> fail (handler exn)


let sequence f lst =
  let rec aux acc = function
    | hd :: tl -> (
        match f hd with
        | Ok x -> aux (x :: acc) tl
        | Errors _ as errs -> errs
      )
    | [] -> ok @@ List.rev acc in
  aux [] lst

let error_pp fmt error =
  Format.fprintf fmt "%s" error.message

let error_pp_short fmt error =
  Format.fprintf fmt "%s" error.title

let errors_pp =
  Format.pp_print_list
    ~pp_sep:Format.pp_print_newline
    error_pp

let errors_pp_short =
  Format.pp_print_list
    ~pp_sep:Format.pp_print_newline
    error_pp_short

let pp_to_string pp () x =
  Format.fprintf Format.str_formatter "%a" pp x ;
  Format.flush_str_formatter ()

let errors_to_string = pp_to_string errors_pp

module Assert = struct
  let assert_true ~msg = function
    | true -> ok ()
    | false -> simple_fail msg

  let assert_equal_int ?msg a b =
    let msg = Option.unopt ~default:"not equal int" msg in
    assert_true ~msg (a = b)

  let assert_list_size ~msg lst n =
    assert_true ~msg (List.length lst = n)

  let assert_list_size_2 ~msg = function
    | [a;b] -> ok (a, b)
    | _ -> simple_fail msg

  let assert_list_size_1 ~msg = function
    | [a] -> ok a
    | _ -> simple_fail msg
end
